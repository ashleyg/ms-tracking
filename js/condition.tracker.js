/**
 * @file
 * Provides condition values for all tracker conditions.
 */

(function (Drupal) {

  Drupal.smartContent = Drupal.smartContent || {};
  Drupal.smartContent.plugin = Drupal.smartContent.plugin || {};
  Drupal.smartContent.plugin.Field = Drupal.smartContent.plugin.Field || {};

  Drupal.smartContent.plugin.Field['tracker:nodes_viewed'] = function (condition) {
    if ('tracked_data' in drupalSettings && 'node.view' in drupalSettings.tracked_data) {
      return Object.keys(drupalSettings.tracked_data['node.view']).length || 0;
    }
    return 0;
  };

})(Drupal);
