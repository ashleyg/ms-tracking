<?php

namespace Drupal\ms_tracking\Plugin\smart_content\Condition;

use Drupal\smart_content\Condition\ConditionTypeConfigurableBase;

/**
 * Provides a default Smart Condition.
 *
 * @SmartCondition(
 *   id = "tracker",
 *   label = @Translation("Tracker"),
 *   group = "tracker",
 *   weight = 0,
 *   deriver = "Drupal\ms_tracking\Plugin\Derivative\TrackerDerivative"
 * )
 */
class TrackerCondition extends ConditionTypeConfigurableBase {

  /**
   * {@inheritdoc}
   */
  public function getLibraries() {
    $libraries = array_unique(array_merge(parent::getLibraries(), ['ms_tracking/condition.tracker']));
    return $libraries;
  }

}
