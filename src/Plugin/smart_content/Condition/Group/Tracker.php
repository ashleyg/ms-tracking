<?php

namespace Drupal\ms_tracking\Plugin\smart_content\Condition\Group;

use Drupal\smart_content\Condition\Group\ConditionGroupBase;

/**
 * Provides a default Smart Condition.
 *
 * @SmartConditionGroup(
 *   id = "tracker",
 *   label = @Translation("Tracker"),
 *   weight = 0,
 * )
 */
class Tracker extends ConditionGroupBase {

}
