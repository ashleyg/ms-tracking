<?php

namespace Drupal\ms_tracking\CacheContext;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\ms_tracking\Tracker;

/**
 * Class TrackerCacheContext.
 */
class TrackerCacheContext implements CacheContextInterface {

  /**
   * Drupal\Core\TempStore\PrivateTempStoreFactory definition.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempstorePrivate;


  /**
   * Constructs a new TrackerCacheContext object.
   */
  public function __construct(PrivateTempStoreFactory $tempstore_private) {
    $this->tempstorePrivate = $tempstore_private->get('ms_tracking.tracker');
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    \Drupal::messenger()->addMessage('Label of cache context');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    // Actual logic of context variation will lie here.
    return serialize($this->tempstorePrivate->get(Tracker::TRACKED_DATA));
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    return new CacheableMetadata();
  }

}
