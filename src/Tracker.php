<?php

namespace Drupal\ms_tracking;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatch;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Routing\RoutingEvents;
use Drupal\node\NodeInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class Tracker.
 */
class Tracker implements EventSubscriberInterface {

  const TRACKED_DATA = 'tracked_data';

  /**
   * Drupal\Core\TempStore\PrivateTempStoreFactory definition.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $tempstore;

  /**
   * Constructs a new Tracker object.
   */
  public function __construct(PrivateTempStoreFactory $tempstore_private) {
    $this->tempstore = $tempstore_private->get('ms_tracking.tracker');
  }

  /**
   * @return array
   */
  static function getSubscribedEvents() {
    $events = [];
    $events[KernelEvents::REQUEST][] = ['onRequest'];
    return $events;
  }

  /**
   * @param Event $event
   */
  public function onRequest(Event $event) {
    // Do stuff
    $request = $event->getRequest();
    $route_match = RouteMatch::createFromRequest($request);
    $route_name = $route_match->getRouteName();
    if ($route_name == 'entity.node.canonical') {
      /** @var \Symfony\Component\HttpFoundation\ParameterBag $parameters */
      $parameters = $route_match->getParameters();
      foreach ($parameters->getIterator() as $param) {
        if ($param instanceof NodeInterface) {
          $this->track('node.view', $param->id());
          break;
        }
      }
    }
  }

  public function set(string $key, string $value) {
    $this->tempstore->set($key, $value);
  }

  public function get(string $key) {
    return $this->tempstore->get($key);
  }

  public function track($track_type, $item) {
    $tracked_data = $this->tempstore->get(self::TRACKED_DATA);
    $tracked_data[$track_type][$item] = $item;
    $this->tempstore->set(self::TRACKED_DATA, $tracked_data);
  }
}
